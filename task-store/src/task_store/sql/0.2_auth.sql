-- CREATE initial auth tables

CREATE TABLE task_store.users(
    id TEXT PRIMARY KEY,
    email TEXT UNIQUE,
    password TEXT,
    created TIMESTAMP DEFAULT NOW()
);

CREATE TABLE task_store.user_token(
    user_id TEXT PRIMARY KEY,
    created TIMESTAMP DEFAULT NOW(),
    token TEXT UNIQUE,
    FOREIGN KEY (user_id) references task_store.users(id)
);

CREATE TABLE task_store.user_keys(
    user_id TEXT PRIMARY KEY,
    private TEXT,
    FOREIGN KEY (user_id) references task_store.users(id)
);


-- Token deletion should happen on a schedule.
-- This could be a cron job in the database
-- or a scheduled container.
