"""API response models."""

from typing import Any, List

from pydantic import BaseModel, Field, ConfigDict

from task_api.models.token import Token


class Link(BaseModel):
    """Link references."""

    href: str
    title: str
    method: str = Field(default = "GET")
    # rel: str
    # responseType: str
    # hrefLang: str

    def model_post_init(self, *args, **kwargs) -> None:
        """Expand href to include host name."""
        ...


class MetaData(BaseModel):
    """This is the Base Response of the API."""

    title: str
    description: str
    url: Link
    links: List[Link | None] = Field(default = [])
    keywords: List[str | None] = Field(default = [])


class Message(BaseModel):
    """Issue a message with a response."""

    status: str
    message: str


class Response(BaseModel):
    """Wrap the metadata and message objects."""

    details: MetaData
    content: Any


class UserContent(BaseModel):
    """User detail."""
    model_config = ConfigDict(extra='allow')

    id: str
    email: str
    name: str
    self: Link
    token: Token | None = Field(default=None)
    client_secret: str | None = Field(default=None)


class Root(MetaData):
    """Root response contains additional MetaData."""

    version: str
    terms_of_service: Link = Field(
        default = Link(
            href="",
            title="Terms of Service"
        )
    )
    support: dict = Field(default = {})
    licence: dict = Field(
        default={
            "name": "Apache 2.0",
            "identifier": "MIT"
        }
    )
