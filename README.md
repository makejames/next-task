# Next-task

A bare bones task management solution for those that have far too much to do.

## Concept

Tasks are a discrete unit of work that needs to be done, such as:

- `email Fred the stats from last quarter`
- `read the article about bees`

Tasks are stateful.
When you have an active task it should stay the next task until it is completed.

## Deployment

**Not supported**

**kubernetes / helm install**

This repository contains the required helm charts
and helm chart references in order to deploy the application.

Secrets should be passed in as command line arguments using the `--set` flag.

```bash
helm install -n next-task next-task .
```

Once everything is up and running, exec into the running database pod.

Create 3 users:

- create-*
- write-*
- read-*

Grant connect and usage to the task database each user.

Grant all privileges to the create user.

```sh
GRANT CONNECT ON DATABASE next_task_store TO {user};
```

Revoke Create for your write and read users
and Grant them usage.


## Development

Check out the [Contributing guide](./CONTRIBUTING.md).

```bash
git clone git@gitlab.com:makejames/next-task.git
```

**Pre-requisites**

- Python >= 3.11
- Node >= 22.0
- `nerdctl` running a rootless `containerd` with `buildkit` enabled

### Dev Dependencies

Before contributing, you must install some development dependencies.

Run `pdm install` in the root of the repository to create a `.venv/`
and install python linters.

Run `npm i` to install html and JavaScript linters.

Install `nerdctl` from the
[pre-built binaries](https://github.com/containerd/nerdctl/releases)
and follow the set-up instructions for initialising rootless containerd.

### Pre-commit

This repository uses [pre-commit hooks](https://pre-commit.com).

It is advised that `pre-commit` is installed as a user installed python app.

```sh
pip install pre-commit
pre-commit install
```

### Pipelines

This project uses gitlab-ci runners.
The `/.gitlab-ci.yml` is the parent pipeline configuration file.
This pipeline triggers the pipelines of each sub-package.
Any common jobs,
or jobs that depend on the success of multiple sub-package pipelines,
should be declared in here.
