-- INIT info schema and version table

CREATE SCHEMA task_store_info;

CREATE TABLE task_store_info.version(
    tag TEXT PRIMARY KEY,
    name TEXT,
    updated TIMESTAMP DEFAULT NOW(),
    content TEXT
);

CREATE SCHEMA task_store;

-- To delete the database
-- DROP SCHEMA task_store_info CASCADE;
-- DROP SCHEMA task_store CASCADE;
