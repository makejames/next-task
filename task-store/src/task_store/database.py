"""Create database connections."""

import psycopg
from typing import Optional

from task_store.environment import Database
from task_store.logging_config import log


class Connection:  # pragma: no cover
    """Create a database connection."""

    def __init__(self) -> None:
        """Instantiate the class."""
        self.conn = psycopg.connect(
            conninfo=Database().connection_string()
        )

    def __enter__(self) -> psycopg.Connection:
        """Return the database connection."""
        return self.conn

    def __exit__(self, err: Optional[str], value: Optional[str],
                 traceback: Optional[str]) -> bool:
        """Return a true-thy value to continue running application."""
        if value:
            log.warning(f"{err}: {value}")
        else:
            self.conn.commit()
            self.conn.close()
        return True
