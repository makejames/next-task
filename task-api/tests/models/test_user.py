"""Test the methods of the user module."""

import pytest

from task_api.models.user import User


class TestUser:
    """Test the methods and validation of the User class."""

    @pytest.mark.parametrize(
        "id,test_email,name,password,expected",
        [
            (
                "test",
                "james@makejames.com",
                "james",
                "password",
                {"id":"test","email":"james@makejames.com","name":"james"}
            ),
        ],
    )
    def test_valid_email_then_pass(self, id, test_email,
                                   name, password, expected) -> None:
        """R-BICEP: Right."""
        test = User(id=id, email=test_email, name=name, password=password)
        assert test.model_dump() == expected

    @pytest.mark.parametrize(
        "test_email",
        [
            ("james@example.com"),
            ("james@local"),
            ("local_test"),
        ],
    )
    def test_invalid_email_then_value_error(self, test_email) -> None:
        """R-BICEP: Error."""
        with pytest.raises(ValueError):
            User(email=test_email, name="A string", password="password")

    @pytest.mark.parametrize(
        "test_pass",
        [
            ("password"),
            ("My password"),
        ],
    )
    def test_password_hash(self, test_pass) -> None:
        """R-BICEP: Right."""
        user = User(email="james@makejames.com", name="A string",
                    password=test_pass)
        user.hash_password().get_secret_value()

    @pytest.mark.parametrize(
        "test_pass,bytes_encoded_hash,expected",
        [
            (
                "password",
                ("$argon2id$v=19$m=65536,t=2,p=1$Km+mXVlyNhjp7tcuxLI8"
                 "7w$OZqh84sulzp3s0MdvUZtFmCB9OovXW31gxlPkt1YJuA"),
                True,
            ),
            (
                "My password",
                ("$argon2id$v=19$m=65536,t=2,p=1$tiOdl+FX1Ya/fpRbI8cM"
                 "yg$yZlbAcl7D/so374dxkFfwUff5Pd++x+3gmmiFksRHiU"),
                True,
            ),
            (
                "My Password",
                ("$argon2id$v=19$m=65536,t=2,p=1$tiOdl+FX1Ya/fpRbI8cM"
                 "yg$yZlbAcl7D/so374dxkFfwUff5Pd++x+3gmmiFksRHiU"),
                False,
            ),
        ],
    )
    def test_password_verify(self, test_pass, bytes_encoded_hash,
                             expected) -> None:
        """R-BICEP: Right."""
        user = User(email="james@makejames.com", name="A string",
                    password=bytes_encoded_hash)
        assert user.validate_password(test_pass) == expected
