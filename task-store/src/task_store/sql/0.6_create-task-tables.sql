-- Create Types

CREATE TYPE task_status AS ENUM (
    'open',
    'closed'
);

-- Create Task table

CREATE TABLE task_store.task (
    id TEXT PRIMARY KEY,
    summary TEXT,
    created DATE NOT NULL DEFAULT NOW(),
    user_id TEXT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES task_store.users(id)
);

CREATE INDEX task_created_by ON task_store.task(user_id);

CREATE TABLE task_store.status (
    task_id TEXT,
    status task_status NOT NULL DEFAULT 'open',
    updated TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    PRIMARY KEY (
        task_id,
        updated
    ),
    FOREIGN KEY (task_id) REFERENCES task_store.task(id)
);

CREATE INDEX task_status_id ON task_store.status(task_id);

-- Update status table

CREATE OR REPLACE FUNCTION CASCADE_TASK_CREATION()
RETURNS TRIGGER AS
$$
    BEGIN
        INSERT INTO task_store.status (task_id) VALUES (NEW.id);
        RETURN new;
    END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER create_task AFTER
INSERT ON task_store.task
FOR EACH ROW
EXECUTE PROCEDURE CASCADE_TASK_CREATION();
