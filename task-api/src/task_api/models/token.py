"""Instantiate the Token class."""

from typing import Self
from secrets import token_urlsafe

from psycopg.rows import class_row
from psycopg import sql
from pydantic import BaseModel, Field
from fastapi import status
from datetime import datetime, timedelta, timezone

from task_api import log
from task_api.services.database import Connection


class Token(BaseModel):
    """A Token Object."""

    user_id: str
    token: str = Field(
        default_factory=lambda: f"{token_urlsafe(64)}--{token_urlsafe(32)}"
    )
    created: datetime = Field(
        default_factory=lambda: datetime.now(timezone.utc)
    )

    def is_valid(self) -> bool:
        """Confirms if the Token is valid."""
        valid_time = datetime.now(timezone.utc) - timedelta(minutes=60)
        if self.created > valid_time:
            log.debug(f"Valid Token already exists. Created: {self.created}. "
                      f"Valid Time from: {valid_time}")
            return True
        return False

    def write(self) -> None:
        """Write the token to the database."""
        log.debug("Writing Token to database.")
        insert = sql.SQL(
            """
                INSERT INTO task_store.user_token (user_id, token, created)
                Values ({}, {}, {})
            """
        ).format(sql.Placeholder(), sql.Placeholder(), sql.Placeholder(),)
        with Connection() as conn:
            with conn.cursor() as cur:
                cur.execute(insert, [self.user_id, self.token, self.created])

    @classmethod
    def read(cls, user_id: str) -> Self | None:
        """Look up existing token."""
        log.debug("Reading Token from database.")
        select = sql.SQL(
            """
                SELECT user_id, token, created
                FROM task_store.user_token
                WHERE user_id={}
                ORDER BY created DESC
                LIMIT 1
            """
        ).format(
            sql.Placeholder()
        )
        with Connection() as conn:
            with conn.cursor(row_factory=class_row(cls)) as cur:
                return cur.execute(select, [user_id]).fetchone()
        return None

    @classmethod
    def create(cls, user_id: str) -> Self:
        """Create a new Token."""
        existing_token = cls.read(user_id)

        if existing_token is not None:
            if existing_token.is_valid():
                status.HTTP_200_OK
                return existing_token

        token: Self = cls(user_id=user_id)
        token.write()
        status.HTTP_201_CREATED
        return token
