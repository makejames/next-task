"""Instantiate the environment variables."""

from os import environ


class Database:
    """Create an instance of the Database Environment Variables."""

    def __init__(self) -> None:
        """Instantiate the class."""
        self.host: str = environ.get("TASK_STORE_DB_HOST", "")
        self.user: str = environ.get("TASK_STORE_DB_USER", "")
        self.password: str = environ.get("TASK_STORE_DB_PASSWORD", "")
        self.database: str = environ.get("TASK_STORE_DB", "next_task_store")
        self.port: str = environ.get("TASK_STORE_DB_PORT", "5432")
        self.protocol: str = environ.get("TASK_STORE_DB_PROTOCOL", "postgresql")

    def connection_string(self) -> str:
        """Returns the combined connection string."""
        return f"{self.protocol}://{self.host}:{self.port}/" \
               f"{self.database}?user={self.user}&password={self.password}"


class Logging:
    """Create an instance of the Logging environment variables."""

    def __init__(self) -> None:
        """Instantiate the class."""
        self.level = environ.get("TASK_STORE_LOGGING_LEVEL", "debug")
