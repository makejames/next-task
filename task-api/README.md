# Task API

The `TaskAPI` is a simple REST API built using `FastAPI`.
It provides endpoints to manage tasks including, creating new tasks,
marking tasks as complete and skipping tasks they don't want to do just yet.

## Features

- **Create Tasks:** easily create new tasks.
- **List Tasks:** request a list of tasks they have to do.
- **Next Task:** request and interact with the Next task.
- **Skip Tasks:** skip tasks you don't want to do right away.
- **Complete Tasks:** once a task is done it can be closed.

## Benefits

**Simplicity**

The API is designed to be straightforward and easy to use.
Users can manage their tasks with limited complexity.

**Flexibility**

Users can interact with the API using any HTTP client.

**Efficiency**

Efficiently create, complete and skip tasks.
With a few simple endpoints, users can streamline their task workflow.

## Dependencies

This service uses `poetry` as its package manager of choice.
Run `poetry install` to install all dependencies.

## Deployment

It is possible to run this service on bare-metal hardware.
The initial approach for this service supports containerised solutions.

**Running locally**

Run the following commands to build the dependencies
and run in the poetry virtual environment.

```sh
poetry build
poetry run uvicorn task_api.app:TaskAPI --host="127.0.0.1" --port="5000" &
```

Adding the `&` will allow the process to run in the background.
You can then use `curl` to test various endpoints.

```sh
curl 127.0.0.1:5000/ | python3 -m json.tool
```
