# Task Store

This package manages upgrade patterns for database updates.

This allows maintainers to make changes to the database in a controlled way.
Inspired by other tools such as Flyway and alembic,
this simple package allows for schema changes to be applied programmatically.

## Structure

Version incremented SQL files are stored in `task_store/sql/`.
These files **MUST** be in the format of `MAJOR.MINOR_change-title.sql`.
This is, version.increment_kebab-case-short-change-name.sql.

Files that do not end in `.sql` will not be processed.
Files that do not follow the version format will not be processed.

## Deployment

## Development
