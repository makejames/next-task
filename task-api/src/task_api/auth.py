"""Authentication Route."""

from typing import Annotated

from fastapi import APIRouter, HTTPException, Header, Path
from pydantic import SecretStr

from task_api.models.responses import (MetaData, Message, Response,
                                       UserContent, Link)
from task_api.models.user import User
from task_api.models.token import Token

from task_api import log


Auth = APIRouter(tags=["Authentication",])

@Auth.get("")
@Auth.get("/")
async def auth(token: str | None = None) -> Response:
    """Authentication Collection."""
    _message=Message(status="Success", message="")
    if token:
        _message = Message(
            status="Success",
            message=f"{token}: Token is valid"
        )

    return Response(
        details=MetaData(
            title="Authentication",
            description="This collection contains endpoints for managing " \
                        "user authentication",
            url=Link(href="/auth", title="Authentication"),
            keywords=["Authentication", "Login"]
        ),
        content=_message
    )

@Auth.put("/user", response_model_exclude_none=True)
async def create_user(email: str, name: str, password: str) -> Response:
    """Create a new user, returns client secret."""

    if existing_user := User.exists(email):
        log.info(f"User: {existing_user.id} already exists.")
        raise HTTPException(status_code=400, detail="Bad Request")

    user = User(email=email, name=name, password=SecretStr(password))

    client_secret = user.create_client_secret()
    user.create_user()

    if user.exists(email):
        return Response(
            details=MetaData(
                title="Create User",
                description="Create a new user, "
                            "operation can be performed only once.",
                url=Link(href="/auth/user", title="Create User"),
                keywords=["Authentication", "Login", "User"],
            ),
            content=UserContent(
                id=user.id,
                email=user.email,
                name=user.name,
                client_secret=client_secret,
                self=Link(
                    href=f"/auth/user/{user.id}",
                    title="Login page for user."
                ),
            )
        )

    log.critical("User creation has failed. Raising 500 error.")
    raise HTTPException(status_code=500, detail="Server Processing Error")

    # generate pub/private keys
    # return user-friendly secret as public key
    # save private key to database


@Auth.get("/user/{user_id}", response_model_exclude_none=True)
@Auth.get("/user/{user_id}/", response_model_exclude_none=True)
async def user(user_id: Annotated[str, Path()],
               password: Annotated[str, Header()],
               client_secret: Annotated[str, Header()]) -> Response:
    """Returns summary information about a user."""
    user = User.find_user_id(user_id, password, client_secret)

    if user is None:
        raise HTTPException(status_code=400, detail="Bad request.")

    return Response(
        details=MetaData(
            title="User Information",
            description="View information about a user.",
            url=Link(href=f"/auth/user/{user.id}", title="User Information"),
            keywords=["Authentication", "Login", "User"],
            links=[
                Link(
                    href=f"/auth/user/{user.id}/token",
                    title="Request a new token for a user."
                ),
            ]
        ),
        content=UserContent(
            id=user.id,
            email=user.email,
            name=user.name,
            self=Link(
                href=f"/auth/user/{user.id}",
                title="Login page for user."
            ),
        )
    )

@Auth.post("/user/{user_id}/token", response_model_exclude_none=True)
async def new_token(user_id: Annotated[str, Path()],
                    password: Annotated[str, Header()],
                    client_secret: Annotated[str, Header()]) -> Response:
    """Either fetches an existing token or generates a new one."""
    user = User.find_user_id(user_id, password, client_secret)

    if user is None:
        raise HTTPException(status_code=400, detail="Bad request.")

    return Response(
        details=MetaData(
            title="Token Creation",
            description="Request a new token.",
            url=Link(href=f"/auth/user/{user.id}/token",
                     title="Request a token"),
            keywords=["Authentication", "Login", "User"],
            links=[]
        ),
        content=UserContent(
            id=user.id,
            email=user.email,
            name=user.name,
            token=Token.create(user_id=user.id),
            self=Link(
                href=f"/auth/user/{user.id}/token",
                title="Request a new token."
            ),
        )
    )
