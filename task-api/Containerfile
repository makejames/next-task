# Containerfile for task-api

ARG PYTHON_BASE=3.11-slim

# Builder Image
FROM python:$PYTHON_BASE AS base

RUN pip install -U --no-cache-dir pdm=="2.22.0"

ENV PDM_CHECK_UPDATE=false

WORKDIR /app

COPY pyproject.toml pdm.lock ./
RUN touch README.md

COPY src/ /app/src

RUN pdm install --check --prod --no-editable

# Run time image
FROM python:$PYTHON_BASE AS runtime

WORKDIR /app

RUN apt update && apt install -y libpq-dev gcc

COPY --from=base /app/.venv /app/.venv

ENV PATH="/app/.venv/bin:$PATH"

EXPOSE 5000

# uvicorn env variables
ENV UVICORN_HOST="0.0.0.0" \
    UVICORN_PORT=5000 \
    UVICORN_LOG_LEVEL=info \
    UVICORN_ROOT_PATH=""

# task-api env variables
ENV TASK_API_NAME="task-api" \
    TASK_API_HOST="" \
    TASK_API_VERSION="" \
    TASK_API_LICENCE="Apache 2.0 | MIT" \
    TASK_API_SUPPORT_NAME="" \
    TASK_API_SUPPORT_CONTACT="" \
    TASK_API_SUPPORT_URL="" \
    TASK_API_DB_HOST="" \
    TASK_API_DB_USER="" \
    TASK_API_DB_PASSWORD="" \
    TASK_API_DB="task_store" \
    TASK_API_DB_PORT="5432" \
    TASK_API_DB_PROTOCOL="postgresql"

COPY src/ /app/

ENTRYPOINT ["uvicorn", "task_api.app:TaskAPI"]
