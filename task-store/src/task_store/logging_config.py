"""Create and configure the application logger."""

import structlog

log = structlog.get_logger()
