"""User related objects."""

from typing import Self
import uuid
import secrets
import hashlib
import time

from email_validator import validate_email
from nacl import pwhash, exceptions
from pydantic import BaseModel, SecretStr, field_validator, Field
from psycopg.rows import class_row
from psycopg import sql

from task_api import resolver, log
from task_api.services.id import Id
from task_api.services.database import Connection


class User(BaseModel):
    """User object."""

    id: str = Field(default_factory=lambda: Id().generate_id())
    email: str
    name: str
    password: SecretStr = Field(exclude=True)
    client_secret: SecretStr = Field(default=SecretStr(""), exclude=True)

    @field_validator("email", mode="before")
    @classmethod
    def email_validate(cls, email: str) -> str:
        """Run checks on email.

        Performs email validation on each instantiation of the class.
        This includes validation of user supplied data
        and validation of database entries.

        The email_validator library performs validation on structure
        and the address itself.
        This includes checking the domain against DNS records and block lists.
        """
        log.debug(f"Running validation checks on email: {email}")
        return validate_email(
            email,
            check_deliverability=True,
            dns_resolver=resolver
        ).normalized.lower()

    def hash_password(self) -> SecretStr:
        """Obfuscate the password using a deterministic hash."""
        _pass = bytes(self.password.get_secret_value(), "utf-8")
        return SecretStr(pwhash.str(_pass).decode("utf-8"))

    def validate_password(self, password_to_check: str) -> bool:
        """Obfuscate the password using a deterministic hash."""
        existing_pass = bytes(self.password.get_secret_value(), "utf-8")
        check_value = bytes(password_to_check, "utf-8")
        try:
            log.debug(f"Validating password for {self.id}")
            return pwhash.verify(existing_pass, check_value)
        except exceptions.InvalidkeyError:
            log.warning(f"Password validation failed for {self.id}")
            return False

    @classmethod
    def find_user_id(cls, user_id: str, password: str,
                     client_secret: str) -> Self | None:
        """Find an existing user."""
        query = sql.SQL(
            """
                SELECT id, email, name, password, secret as client_secret
                FROM task_store.users
                WHERE id={}
            """
        ).format(
            sql.Placeholder()
        )
        with Connection() as conn:
            with conn.cursor(row_factory=class_row(cls)) as cur:
                user = cur.execute(query, [user_id]).fetchone()
            user = user
        if user is None:
            log.info(f"{user_id} not found")
            return None

        if user.client_secret.get_secret_value() != client_secret:
            log.info(f"Secret for {user_id} does not match stored secret.")
            return None

        if user.validate_password(password) is False:
            log.info(f"Password for {user_id} does not match provided.")
            return None

        return user

    @classmethod
    def exists(cls, email) -> Self | None:
        """Checks the database and confirms if the user exists."""
        query = sql.SQL(
            """
                SELECT id, email, name, password
                FROM task_store.users
                WHERE
                    email={email}
            """).format(
                email=email,
            )
        with Connection() as conn:
            with conn.cursor(row_factory=class_row(cls)) as cur:
                user = cur.execute(query).fetchone()
            return user
        return None

    def create_client_secret(self) -> str:
        """Creates the client secret."""
        entropy = str(uuid.uuid4().int).encode("utf-8") \
                  + secrets.token_hex(64).encode("utf-8") \
                  + str(time.time()).encode('utf-8')
        client_secret = hashlib.sha256(entropy).hexdigest()
        self.client_secret = SecretStr(client_secret)
        return client_secret

    def create_user(self) -> None:
        """Create a user."""
        hash_password = self.hash_password().get_secret_value()
        query = sql.SQL(
            """
                INSERT INTO task_store.users
                    (id, email, password, name, secret)
                VALUES
                    ({id}, {email}, {password}, {name}, {client_secret})
            """).format(
                id=self.id,
                email=self.email,
                password=hash_password,
                name=self.name,
                client_secret=self.client_secret.get_secret_value(),
            )

        with Connection() as conn:
            with conn.cursor() as cur:
                cur.execute(query)
