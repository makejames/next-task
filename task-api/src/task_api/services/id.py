"""Module for generating User Ids."""

from shortuuid import ShortUUID


class Id:
    """Generate and Manage Ids."""

    def __init__(self) -> None:
        """Instantiate the class."""
        self.cache: list[str] = []

    def generate_id(self) -> str:
        """Generates a new unique Id."""
        new_id = ShortUUID().random(length=10)  # 52 billion combinations
        count = 0
        while new_id in self.cache:
            new_id = ShortUUID().random(length=10)
            if count > 300:
                raise ValueError("Cannot generate new Id.")
            count += 1
        self.cache.append(new_id)
        return f"{new_id}"

    # TODO: populate cache with actual database values on startup
