"""Test the app end-points."""

from fastapi.testclient import TestClient

from task_api.app import TaskAPI


class TestApp:
    """Test the application endpoints."""

    def test_root_endpoint(self) -> None:
        """R-BICEP: Right."""
        with TestClient(TaskAPI) as client:
            response = client.get("/")
            assert response.status_code == 200

    def test_api_endpoint(self) -> None:
        """R-BICEP: Right."""
        with TestClient(TaskAPI) as client:
            response = client.get("/api")
            assert response.status_code == 200
            assert response.json() == TaskAPI.openapi_schema
