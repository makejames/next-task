-- Considering how expired tokens are managed

ALTER TABLE task_store.user_token
DROP CONSTRAINT user_token_pkey;

ALTER TABLE task_store.user_token
ADD CONSTRAINT user_token_pkey PRIMARY KEY (user_id, created);

CREATE INDEX idx_task_store_user_id
ON task_store.user_token (user_id);

ALTER TABLE task_store.user_token
ALTER COLUMN created TYPE TIMESTAMP WITH TIME ZONE;
