{{/* vim: set filetype=mustache: */}}

{{/*
Expand the name of the chart.
*/}}
{{- define "postgres.name" -}}
  {{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "postgres.database" }}
  {{- default "task" .Values.global.databaseName }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "postgres.fullname" -}}
  {{- if .Values.fullnameOverride }}
    {{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
  {{- else }}
    {{- $name := default .Chart.Name .Values.nameOverride }}
    {{- if contains $name .Release.Name }}
      {{- .Release.Name | trunc 63 | trimSuffix "-" }}
    {{- else }}
      {{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
    {{- end }}
  {{- end }}
{{- end }}

{{/* Qualify Image Name */}}
{{- define "postgres.image" }}
  {{- if .Values.imageOverride }}
    {{- .Values.imageOverride }}
  {{- else }}
    {{- with .Values.image }}
      {{- printf "%s/%s:%s" .registry .repository .tag }}
    {{- end }}
  {{- end }}
{{- end }}

{{/* Declare Secrets Name */}}
{{- define "postgres.secrets" }}
  {{- printf "%s-secrets" ( include "postgres.fullname" . ) }}
{{- end }}

{{/* Declare Service Name */}}
{{- define "postgres.serviceName" }}
  {{- printf "%s-svc" ( include "postgres.fullname" . ) }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "postgres.chart" -}}
  {{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "postgres.labels" -}}
helm.sh/chart: {{ include "postgres.chart" . }}
{{ include "postgres.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
chart_name: {{ .Chart.Name }}
chart_version: {{ .Chart.Version | quote }}
app_version: {{ .Chart.AppVersion | quote }}
date: {{ now | htmlDate }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "postgres.selectorLabels" -}}
app.kubernetes.io/name: {{ include "postgres.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "postgres.serviceAccountName" -}}
  {{- if .Values.serviceAccount.create }}
    {{- default (include "postgres.fullname" .) .Values.serviceAccount.name }}
  {{- else }}
    {{- default "default" .Values.serviceAccount.name }}
  {{- end }}
{{- end }}

{{/* Declare postgres user */}}
{{- define "postgres.adminUser" }}
  {{- if .Values.auth.postgres.nameOverride }}
    {{- .Values.auth.postgres.nameOverride }}
  {{- else if .Values.global.postgres.nameOverride }}
    {{- .Values.global.postgres.nameOverride }}
  {{- else }}
    {{- "postgres" }}
  {{- end }}
{{- end }}
