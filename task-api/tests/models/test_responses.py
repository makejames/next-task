"""Test the models module."""


from task_api.models.responses import MetaData, Link


class TestResponseModels:
    """Test the models of the Response module."""

    def test_metadata_response(self) -> None:
        """R-BICEP: Right."""
        expected = {
            "title": "test_api",
            "description": "test api response",
            "url": {
                "href": "/test",
                "method": "GET",
                "title": "test"
            },
            "links": [],
            "keywords": []
        }
        _self = Link(
            href="/test",
            title="test"
        )
        test = MetaData(
            title="test_api",
            description="test api response",
            url=_self,
            links=[],
            keywords=[]
        )
        assert test.model_dump() == expected
