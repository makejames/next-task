"""Instantiate the task_api package."""

# ruff: noqa: F401

from task_api.logging_config import log

from task_api.models.env import Env

from email_validator import caching_resolver

resolver = caching_resolver(timeout=10)
