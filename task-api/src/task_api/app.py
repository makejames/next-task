"""Declares the endpoints for the service."""

import structlog
from fastapi import FastAPI, Request, Response
import uuid

from task_api import Env, log
from task_api.models.responses import Root, Link
from task_api.auth import Auth
from task_api.tasks import Task


env = Env()

description = """
The **TaskAPI** is a RESTful API built using FastAPI.
It provides endpoints to manage tasks including, creating new tasks,
marking tasks as complete and skipping tasks they don't want to do just yet.

## Features

- **Create Tasks:** easily create new tasks.
- **List Tasks:** request a list of tasks they have to do.
- **Next Task:** request and interact with the Next task.
- **Skip Tasks:** skip tasks you don't want to do right away.
- **Complete Tasks:** once a task is done it can be closed.
"""

TaskAPI = FastAPI(
    title=env.name,
    summary="Tasks as simple as a thought.",
    decription=description,
    version=env.version,
    terms_of_service="",
    licence_info={
        "name": "Apache 2.0",
        "identifier": "MIT"
    },
    openapi_url="/api",
    docs_url="/docs", # Swagger Docs
    redoc_url=None, # Disable redoc
)


@TaskAPI.middleware("http")
async def logger_middleware(request: Request, call_next):
    # Clear previous context variables
    structlog.contextvars.clear_contextvars()

    # Bind new variables identifying the request and a generated UUID
    structlog.contextvars.bind_contextvars(
        path=request.url.path,
        method=request.method,
        request_id=str(uuid.uuid4()),
    )

    # Make the request and receive a response
    response: Response = await call_next(request)
    # Bind the status code of the response
    structlog.contextvars.bind_contextvars(
        status_code=response.status_code,
    )

    if 400 <= response.status_code < 500:
        log.warn("Client error")
    elif response.status_code >= 500:
        log.error("Server error")
    else:
        log.info("OK")

    return response


@TaskAPI.get("/")
async def task_api() -> Root:
    """Root endpoint for the task_api."""
    open_api = Link(href="/api", title="OpenAPI json schema")
    auth = Link(href="/auth", title="Authentication")

    return Root(
        title=env.name,
        description="Tasks as simple as a thought.",
        url=Link(href="/", title=env.name),
        links=[open_api, auth],
        keywords=["tasks", "api",],
        version=env.version,
        support=env.support,
    )


TaskAPI.include_router(router=Auth, prefix="/auth")
TaskAPI.include_router(router=Task, prefix="/tasks")
