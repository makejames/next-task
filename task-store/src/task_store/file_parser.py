"""File object handling."""

from typing import List
from dataclasses import dataclass
from pathlib import Path


class File:
    """File class."""

    def __init__(self, file: str | Path) -> None:
        """Create File Class."""
        self.file_path: Path = Path(file)
        self.suffix: str = self.file_path.suffix
        self.file_name: str = self.file_path.stem
        self.content: str = self.file_path.read_text()
        self.version, self.name = self.file_name.split("_")

    def is_sql(self) -> bool:
        """Confirm file is .sql."""
        if self.file_path.suffix == ".sql":
            return True
        return False

    def parse_version(self) -> tuple:
        """Return semvar increment as tuple."""
        major, minor = self.version.split(".")
        return int(major), int(minor)

    def dict(self) -> dict:
        """Return a dictionary object."""
        return {
            "name": self.name,
            "version": self.version,
            "content": self.content
        }


@dataclass
class Version:
    """Version class."""

    tag: str

    def tuple(self) -> tuple:
        """Extract version string."""
        major, minor = self.tag.split(".")
        return int(major), int(minor)


def collect_files(current: Version, store: str) -> List[File] :
    """Map directory."""
    path = Path(store)
    files = []

    for file in path.iterdir():
        try:
            _file = File(file)
        except ValueError:
            continue

        if not _file.is_sql():
            continue

        if current.tuple() < _file.parse_version():
            files.append(_file)

    return sorted(files, key=lambda x: x.parse_version())
