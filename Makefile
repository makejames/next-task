SHELL=/bin/bash
EXECUTE=pdm run

# Standard
all: build

lint:
	$(MAKE) -C task-api lint
	$(MAKE) -C task-store lint
	$(MAKE) -C ui lint

test:

clean:
	rm -rf .mypy_cache .pdm-build .pytest_cache .ruff_cache .venv
	rm -rf node_modules
	$(MAKE) -C task-api clean
	$(MAKE) -C task-store clean
	$(MAKE) -C ui clean

# less standard
build:

dev:
	pdm install
	npm install
	$(MAKE) -C task-api dev
	$(MAKE) -C task-store dev
	$(MAKE) -C ui dev
