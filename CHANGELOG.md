# CHANGELOG

## 0.10.0 - Create a User

- Create a user
- Store user details in the database.

## 0.9.0 - Versioned Database

- Creates an init image that runs any schema updates before the app
- Implements the dependent charts for postgres

## 0.8.0 Task API

- Create `hello-world` task api application.
- Create initial `helm` deployment boiler plate

## 0.7.0 Task Models

### Changes

- Create a model library reflecting the database structure
- Gitlab ci file re-instated

## 0.6.0 Containerise the package
*2022-12-27*

### Breaking changes
Install targets will not work from root of repository.
Gitlab ci yml file removed

### Changes

- Instantiate a Docker enabled mono repo structure for the package
- Top level Makefile

```sh
    next-task
    |---dockerfiles
    |    |---Dockerfile-package
    |---docs
    |---library
    |    | # shared libraries and models
    |---services
    |    | # applications packages
    |    |---package
    |    |    |---src
    |    |    |    |---__init__.py
    |    |    |    | # package repository
    |    |    |---tests
    |    |    |    |---__init__.py
    |    |    |    | # test repository
    |    |    |---pyproject.toml
    |    |    |---Makefile
    |    |    |---README.md
    |--docker-compose.yaml
    |--Makefile
    |--README.md
```

## 0.5.1 Fix byte formatted strings error
*2022-10-31*

Identified that in some systems the unix encoding of strings gets handled as a bytes instance rather than
a string, this results in a `TypeError` which prevents usability of the app.

### Changes
Moves the `TypeError` to within the appplication
- Task dataclass now has logic to decode byte objects
- if the task summary is not a string or a byte then `TypeError` is raised

## 0.5.0 Postgres Update
*2022-10-25*

Having proved that database storage was infact the way to go, this update provides the well needed move
to a Postgres database.

### Breaking changes
- Tasks will not be migrated from the sqlite solution

### Major Changes
- Task store now in Postgres
- Tasks now always belong to a project and a project 0 is created on setup to hold the project task list
    - this project is (nearly) invisible and is the default task list
    - this project will be set with you use target `Next -c` or `Next -p 0`
    - If you are listing projects, this project will be filtereed out

### Minor Chnages
- Fixes to task list returning non-project tasks or getting stuck
- Fixes to the ci-pipeline
- Fix to project cretion that was reuslting in a `NoneType Error`
- Targetted improvements to the SQL for readability and performance 

## 0.4.0 Sqlite Update
*2022-10-09*

Major refactor of logic models, moving task storage into an sqlite database.
Moving the task storage into a database simplies the implementation and enables the creation of pojects

### Breaking changes
- previously created tasks will be lost

### Major changes:
- Task store managed in a sqlite database stored in the `$HOME/Notes/` directory
- Seamless setup of a task database and required tables on startup

### Minor changes:
- Task addition, fetching, skipping and closing are mutually exclusive activities
- Prioirity now calculated as a delta between task_id and number of skips affecting the issue's 'rank'

### Store Functions
- [x] Establish if file exists if not create and setup schema
- [x] Establish if the database is on the latest version
- [x] Manage Database conections within a context manager to gracefuly close connections
- [x] Wrap DB connections to read or write functions with reutrns tailored to the function.

#### Main Functions
- [x] Task addition
- [x] Task skipping
- [x] Task completion
- [x] Retrieve next task
- [x] Create Project
- [x] set current project
- [x] clear current project
- [x] Declare current project

#### Streach
- [x] Refactor Console Output
- [x] Dynamic project lookup to find projects in the db

### Descoped
- [ ] Retain task data on update
- [ ] when no more tasks prompt user to clear or close current project
