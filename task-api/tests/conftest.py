"""Mange fixtures and configuration for pystest."""

import pytest
import psycopg


@pytest.fixture
def mock_psycopg(mocker, request):
    """Mock the search method of the lookup class."""
    class MockPsycopg:
        def __init__(connection_string=None, conninfo=None):
            return
        def __enter__(self):
            return self
        def __exit__(self, exception_type, exception_value,
                     exception_traceback):
            return
        def cursor(self, row_factory=None):
            return self
        def commit(self,):
            return self
        def close(self):
            return self
        def execute(self, query, data=None):
            # could return query to template out sql
            return self
        def fetchone(self):
            return request.param[0]
        def fetchall(self):
            return request.param

    mocker.patch.object(psycopg, "connect", MockPsycopg)
