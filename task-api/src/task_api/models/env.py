"""Model the run-time environment."""

import os


class Env:
    """Instantiates relevant environment variables."""

    def __init__(self) -> None:
        self.name = os.environ.get("TASK_API_NAME", "task-api")
        self.host = os.environ.get("TASK_API_HOST", "")
        self.version = os.environ.get("TASK_API_VERSION", "0.1.0")
        self.licence = os.environ.get("TASK_API_LICENCE", "")
        self.support = {
            "name": os.environ.get("TASK_API_SUPPORT_NAME", ""),
            "contact": os.environ.get("TASK_API_SUPPORT_CONTACT", ""),
            "url": os.environ.get("TASK_API_SUPPORT_URL", "")
        }
        self.db = {
            "host": os.environ.get("TASK_API_DB_HOST", ""),
            "user": os.environ.get("TASK_API_DB_USER", ""),
            "password": os.environ.get("TASK_API_DB_PASSWORD", ""),
            "database": os.environ.get("TASK_API_DB", "task_store"),
            "port": os.environ.get("TASK_API_DB_PORT", "5432"),
            "protocol": os.environ.get("TASK_API_DB_PROTOCOL", "postgresql")
        }

    def db_connection_string(self) -> str:
        """Returns a connection path to the database."""
        db = self.db
        return f"{db['protocol']}://{db['host']}:{db['port']}" \
               f"/{db['database']}?user={db['user']}&password={db['password']}"
