"""Test the methods of the file_parser module."""

import pytest

from task_store.file_parser import File


class TestFile:
    """Test the methods of the File class."""


    @pytest.mark.parametrize(
        "file,is_sql",
        [
            ("1_test.sql", True),
            ("2_test.py", False),
            ("4_test.sql.txt", False),
            ("2_test.txt", False),
            ("sql_file", False),
        ]
    )
    def test_sql_idenficiation(self, tmp_path, file, is_sql) -> None:
        """R-BICEP: Right."""
        d = tmp_path
        p = d / file
        p.write_text("test_file")
        path = f"{d}/{file}"
        test = File(path)
        assert test.is_sql() == is_sql

    @pytest.mark.parametrize(
        "file",
        [
            (1),
            (None),
            (True),
        ]
    )
    def test_boundary_sql_idenficiation(self, file) -> None:
        """R-BICEP: Boundary."""
        with pytest.raises(TypeError):
            File(file)

    @pytest.mark.parametrize(
        "file,version",
        [
            ("1.0_test.sql", "1.0"),
            ("2.10_test.sql", "2.10"),
            ("3.245_test.sql", "3.245"),
        ]
    )
    def test_version_idenficiation(self, tmp_path, file, version) -> None:
        """R-BICEP: Right."""
        d = tmp_path
        p = d / file
        p.write_text("test_file")
        path = f"{d}/{file}"
        test = File(path)
        assert test.version == version

    @pytest.mark.parametrize(
        "file,expected",
        [
            ("1.0_test.sql", "1.0"),
            ("2.10_test.sql", "2.10"),
            ("3.245_test.sql", "3.245"),
        ]
    )
    def test_dict_method(self, tmp_path, file, expected) -> None:
        """R-BICEP: Right."""
        d = tmp_path
        p = d / file
        p.write_text("test_file")
        path = f"{d}/{file}"
        test = File(path)
        assert test.dict()["version"] == expected

    @pytest.mark.parametrize(
        "file",
        [
            ("1.0_test_string.sql"),
        ]
    )
    def test_string_split_method(self, tmp_path, file) -> None:
        """R-BICEP: Right."""
        d = tmp_path
        p = d / file
        p.write_text("test_file")
        path = f"{d}/{file}"
        with pytest.raises(ValueError):
            File(path)
