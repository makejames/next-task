# Next Task

This sub-package is the front-end application for the service.
This is a simple `nginx` image that serves static content for the site.

## Local deployment

As static html, there is a variety of available methods to serve this content.
For ease, the `python3 -m http.server` command is mapped to the build make target.

```sh
$ make build
python3 -m http.server -b 127.0.0.1 8080 -d src
```

## Updating the version

Pass the updated version increment to the update make target.

```sh
$ make update 0.1
# or
$ make update "0.2"
```
