"""Main run time application."""

from psycopg.rows import class_row
import sys
from task_store.database import Connection
from task_store.logging_config import log
from task_store.file_parser import collect_files, Version


SQL_STORE = "task_store/sql"

if __name__ == "__main__":

    log.debug("Setting up environment.")

    current_version = Version("0.0")

    with Connection() as conn:
        with conn.cursor(row_factory=class_row(Version)) as cur:
            _version = cur.execute("""
                SELECT tag FROM task_store_info.version
                ORDER BY updated DESC
                LIMIT 1;
            """
            ).fetchone()
        if _version:
            current_version = _version

    log.info(f"Current Version: {current_version.tag}")
    log.debug("Fetching upgrade list.")

    upgrade_list = collect_files(current_version, SQL_STORE)

    if upgrade_list == []:
        log.info("No new files to process.")
        sys.exit()

    for file in upgrade_list:
        with Connection() as conn:
            with conn.cursor() as cur:
                log.info(f"Performing upgrade {file.version}: {file.name}")
                cur.execute(file.content)

            conn.commit()

            with conn.cursor() as cur:
                cur.execute(
                    """
                        INSERT INTO
                            task_store_info.version ( tag, name, content )
                        VALUES
                            ( %s, %s, %s )
                    """,
                    (file.version, file.name, file.content)
                )
