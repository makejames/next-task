# Contributing

Contributions are welcome.

Please fork the repository and submit a Pull Request.

## Versioning

Version increments are defined as Major.Minor.Patch

## Lint and Test

Code should be compliant with PEPs 8, 256, 484 and 526.
Unit test coverage should be 85% or higher.

```bash
make check # calls make lint; make test
make coverage # returns the coverage report
```

### Other Make targets

There are several other functional and quality of life targets maintained.

These include, `dev`, `clean` and `test`.

Please check the [Makefile.sample](./Makefile.sample)
for a definitive list.

## Container files

Container files are run-time agnostic.
The naming convention is to refer to a `Containerfile`.

These files are OCI-compatable
and conform to the [Open Containers Image spec](https://github.com/opencontainers/image-spec/blob/main/spec.md)

## Commit Messages

Commit messages are prefixed with the following stubs

```bash
INIT # structural changes to pakage contents
FUNC # functional changes
DOCS # documentation
TEST # commits adding tests to the repository
LINT # corrections to formatting or spelling
REFACTOR # Non functional changes to functions improving performance or readability
```

## Merge Checklist:

**Pre-merge**

- Unit tests are written and coverage is at 85%
- README and Documentation is up to date
- Ensure relevant `pyproject.toml` and `VERSION` files are up to date.

**Post-merge**

- feature branch is deleted
- tag is created on `Main` and pushed to the remote

